import logging
import os
from contextlib import contextmanager
from typing import Generator

from google.cloud import storage
from google.cloud.client import Client
from google.cloud.storage.bucket import Bucket
from google.oauth2 import service_account

logger = logging.getLogger(__name__)


@contextmanager
def get_client(credentials_dir: str) -> Generator[Client, None, None]:
    credentials = service_account.Credentials.from_service_account_file(
        filename=credentials_dir,
        scopes=["https://www.googleapis.com/auth/devstorage.full_control"],
    )
    try:
        client = storage.Client(credentials=credentials)
        yield client
    except Exception as ex:
        logging.error(ex)
        raise ex
    finally:
        client.close()


def upload_folder(bucket: Bucket, folder_dir: str, folder_to_upload=""):
    for root, _, files in os.walk(folder_dir):
        for file in files:
            relative_path = os.path.relpath(os.path.join(root, file), folder_dir)
            blob = bucket.blob(os.path.join(folder_to_upload, relative_path))
            blob.cache_control = "no-cache,no-store,max-age=0"
            blob.upload_from_filename(os.path.join(root, file))


def delete_folder(bucket: Bucket, folder_to_delete: str):
    blobs = bucket.list_blobs(prefix=folder_to_delete)
    for blob in blobs:
        blob.delete()
