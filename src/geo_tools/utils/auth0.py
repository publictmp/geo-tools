import json
import logging
from typing import Literal

import requests

logger = logging.getLogger(__name__)


def call_service(
    method: Literal["get", "post", "patch"],
    url: str,
    data: dict = {},
    token: str = None,
):
    headers = {}

    if token:
        headers = {
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json",
            "Cache-Control": "no-cache",
        }

        data = json.dumps(data)

    res = requests.request(method, url, data=data, headers=headers)
    if not res.ok:
        raise Exception("Error calling service: " + res.text)
    return res.json()


def get_management_token(domain: str, client_id: str, client_secret: str):
    response = call_service(
        "post",
        f"https://{domain}/oauth/token",
        data={
            "grant_type": "client_credentials",
            "client_id": client_id,
            "client_secret": client_secret,
            "audience": f"https://{domain}/api/v2/",
        },
    )
    return response.get("access_token")


def add_callback_url(access_token: str, domain: str, client_id: str, path: str):
    client_resource_uri = f"https://{domain}/api/v2/clients/{client_id}"
    app_definition = call_service("get", client_resource_uri, token=access_token)

    new_app_definition = {}

    if path not in app_definition.get("callbacks", []):
        new_app_definition["callbacks"] = app_definition.get("callbacks", []) + [path]

    if path not in app_definition.get("allowed_logout_urls", []):
        new_app_definition["allowed_logout_urls"] = app_definition.get(
            "allowed_logout_urls", []
        ) + [path]

    if not new_app_definition:
        logger.info(f"The {path} already is part of the client, skipping")
        return

    return call_service(
        "patch", client_resource_uri, token=access_token, data=new_app_definition
    )


def remove_callback_url(access_token: str, domain: str, client_id: str, path: str):
    client_resource_uri = f"https://{domain}/api/v2/clients/{client_id}"
    app_definition = call_service("get", client_resource_uri, token=access_token)

    new_app_definition = {}

    if path in app_definition.get("callbacks", []):
        new_app_definition["callbacks"] = [
            url for url in app_definition.get("callbacks", []) if not url == path
        ]

    if path in app_definition.get("allowed_logout_urls", []):
        new_app_definition["allowed_logout_urls"] = [
            url
            for url in app_definition.get("allowed_logout_urls", [])
            if not url == path
        ]

    if not new_app_definition:
        logger.info(f"The {path} is not part of the client, skipping")
        return

    return call_service(
        "patch", client_resource_uri, token=access_token, data=new_app_definition
    )
