import logging
import sys
from pathlib import Path

import typer
from google.api_core.exceptions import NotFound
from typing_extensions import Annotated

from .utils import auth0, storage

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler(sys.stdout))
app = typer.Typer()


@app.command(name="deploy-ui")
def deploy(
    app_name: Annotated[str, typer.Argument(help="Application's name")],
    app_dir: Annotated[str, typer.Argument(help="Folder to deploy")],
    credentials: Annotated[
        Path, typer.Option(help="Google Service Account credentials json file")
    ],
    branch: Annotated[str, typer.Argument(help="Branchs's name")] = "main",
    update_auth0: Annotated[
        bool, typer.Option(help="Add the callback url to auth0")
    ] = True,
    deploy_url: Annotated[
        str,
        typer.Option(
            help="Deploy URL example: https://test.apps.geospan.com",
        ),
    ] = "",
    auth0_domain: Annotated[
        str, typer.Option(help="Auth0 Domain", envvar="AUTH0_DOMAIN")
    ] = "",
    auth0_admim_id: Annotated[
        str,
        typer.Option(
            help=(
                "Auth0 Client Id, which is enable to machine to machine communitation"
            ),
            envvar="AUTH0_ADMIN_ID",
        ),
    ] = "",
    auth0_admin_secret: Annotated[
        str,
        typer.Option(
            help="Auth0 Client Secret, which is enable to machine to machine communication",
            envvar="AUTH0_ADMIN_SECRET",
        ),
    ] = "",
    auth0_client_id: Annotated[
        str,
        typer.Option(
            help="Auth0 Client Id, that will be updated", envvar="AUTH0_CLIENT_ID"
        ),
    ] = "",
):
    """
    Deploys a folder to bucket and also configures a callback url
    """
    with storage.get_client(credentials_dir=credentials) as client:
        try:
            bucket = client.get_bucket(app_name)
        except NotFound:
            logger.info(f"bucket {app_name} missing, creating bucket")
            bucket = client.create_bucket(app_name)

        storage.upload_folder(bucket, app_dir, branch)

    if not update_auth0:
        return

    try:
        access_token = auth0.get_management_token(
            auth0_domain, auth0_admim_id, auth0_admin_secret
        )
        callback_url = f"{deploy_url}/{branch}"
        auth0.add_callback_url(
            access_token, auth0_domain, auth0_client_id, callback_url
        )
    except Exception as ex:
        logger.error(ex)
        raise ex


@app.command(name="undeploy-ui")
def undeploy(
    app_name: Annotated[str, typer.Argument(help="Application's name")],
    credentials: Annotated[
        Path, typer.Option(help="Google Service Account credentials json file")
    ],
    branch_name: Annotated[str, typer.Argument(help="Name of the branch")] = "main",
    update_auth0: Annotated[
        bool, typer.Option(help="Add the callback url to auth0")
    ] = True,
    deploy_url: Annotated[
        str,
        typer.Option(
            help="Deploy URL example: https://test.apps.geospan.com",
        ),
    ] = "",
    auth0_domain: Annotated[
        str, typer.Option(help="Auth0 Domain", envvar="AUTH0_DOMAIN")
    ] = "",
    auth0_admim_id: Annotated[
        str,
        typer.Option(
            help="Auth0 Client Id, which is enable to machine to machine communitation",
            envvar="AUTH0_ADMIN_ID",
        ),
    ] = "",
    auth0_admin_secret: Annotated[
        str,
        typer.Option(
            help="Auth0 Client Secret, which is enable to machine to machine communication",
            envvar="AUTH0_ADMIN_SECRET",
        ),
    ] = "",
    auth0_client_id: Annotated[
        str,
        typer.Option(
            help="Auth0 Client Id, that will be updated", envvar="AUTH0_CLIENT_ID"
        ),
    ] = "",
):
    """
    Undeploys a folder to bucket and also removes the callback url
    """
    with storage.get_client(credentials_dir=credentials) as client:
        try:
            bucket = client.get_bucket(app_name)
        except NotFound:
            logger.info(f"bucket {app_name} missing, creating bucket")
            bucket = client.create_bucket(app_name)

        storage.delete_folder(bucket, branch_name)

    if not update_auth0:
        return

    try:
        access_token = auth0.get_management_token(
            auth0_domain, auth0_admim_id, auth0_admin_secret
        )
        callback_url = f"{deploy_url}/{branch_name}"
        auth0.remove_callback_url(
            access_token, auth0_domain, auth0_client_id, callback_url
        )
    except Exception as ex:
        logger.error(ex)
        raise ex


@app.callback()
def main(verbose: Annotated[bool, typer.Option("--verbose", "-v")] = False):
    if verbose:
        logger.setLevel(logging.DEBUG)


if __name__ == "__main__":
    app()
